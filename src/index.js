import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

class PassDefaultValue extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      captcha: "",
      password: "",
      bool: true,
    };
  }
  changeValue = (e) => {
    let temp = e.target.name;
    this.setState({ [temp]: e.target.value });
  };
  submitForm = (e) => {
    e.preventDefault();
    if (this.state.bool) {
      this.setState({
        firstName: "Alpha",
        lastName: "Beta",
        email: "example@address.com",
        captcha: "QWE!@pP",
        password: "TopSecret",
        bool: false,
      });
    } else {
      this.setState({
        firstName: "",
        lastName: "",
        email: "",
        captcha: "",
        password: "",
        bool: true,
      });
    }
  };
  render() {
    return (
      <>
        <p>"We are inside parent Component"</p>
        <form onSubmit={this.submitForm}>
          <input type="submit" value="pass as props" />
        </form>
        <p>"We are inside child Component"</p>
        <App data={this.state} />
      </>
    );
  }
}

ReactDOM.render(
  <React.StrictMode>
    <PassDefaultValue />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
