import React from "react";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: this.props.data.firstName,
      lastName: this.props.data.lastName,
      email: this.props.data.email,
      password: this.props.data.password,
      captcha: this.props.data.captcha,
    };
  }

  changeValue = (e) => {
    if (this.props.data.value !== this.state.value) {
      this.setState({ bool: true });
    }
    let temp = e.target.name;
    this.setState({ [temp]: e.target.value });
  };
  submit = (e) => {
    e.preventDefault();
    alert(this.state.firstName);
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.props.data !== prevProps.data) {
      this.setState({
        firstName: this.props.data.firstName,
        lastName: this.props.data.lastName,
        email: this.props.data.email,
        password: this.props.data.password,
        captcha: this.props.data.captcha,
      });
    }
  }

  render() {
    return (
      <form onSubmit={this.submit}>
        First Name{" "}
        <input
          onChange={this.changeValue}
          type="text"
          placeholder="Enter your first name"
          name="firstName"
          value={this.state.firstName}
        />
        <br />
        Last Name{" "}
        <input
          onChange={this.changeValue}
          type="text"
          placeholder="Enter your last name"
          name="lastName"
          value={this.state.lastName}
        />
        <br />
        Email{" "}
        <input
          onChange={this.changeValue}
          type="text"
          placeholder="Enter your email"
          name="email"
          value={this.state.email}
        />
        <br />
        Password{" "}
        <input
          onChange={this.changeValue}
          type="password"
          placeholder="Enter your Password"
          name="password"
          value={this.state.password}
        />
        <br />
        captcha{" "}
        <input
          onChange={this.changeValue}
          type="text"
          placeholder="Enter your name"
          name="captcha"
          value={this.state.captcha}
        />
        <br />
        <input type="submit" value="submit" />
      </form>
    );
  }
}

export default App;
